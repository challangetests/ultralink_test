<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DepositController;
use App\Http\Controllers\TransferController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::prefix('deposit')->group(function () {
        Route::post('/', [DepositController::class, 'store']);
        Route::put('/{code}/confirm', [DepositController::class, 'confirm']);
    });
    Route::prefix('transfer')->group(function () {
        Route::post('/', [TransferController::class, 'store']);
        Route::put('/{code}/confirm', [TransferController::class, 'confirm']);
    });
});
