<?php

namespace App\Helpers;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthHelper
{
    public static function generateAcessToken(User $user): string 
    {
        $user->tokens()->delete();
        return $user->createToken('AcessToken', ['*'], now()->addDay())->plainTextToken;
    }
    
    public static function login(array $data): ?string 
    {
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            $user = Auth::user();
            return AuthHelper::generateAcessToken($user);
        }

        return null;
    }
}