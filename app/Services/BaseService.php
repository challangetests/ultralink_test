<?php

namespace App\Services;

use Exception;
use Illuminate\Support\Facades\Schema;

class BaseService
{
    protected $model;

    public function toOptions(string $field = "name") {
        $result = $this->model::select(['id', $field])->get();
        return $result;
    }
    
    public function getByCustom(array $filters) {
        $query = $this->model::query();

        foreach ($filters as $filter) {
            $model = new $this->model();
            if (!Schema::hasColumn($model->getTable(), $filter['column'])) {
                throw new Exception(__('crud.message.property_not_found'));
            }
            $mod = isset($filter['mod']) ? $filter['mod'] : '=';
            switch ($mod ) {
                case 'or':
                    $query->orWhere($filter['column'], '=', $filter['value']); 
                    break;  
                case 'in':
                    $query->whereIn($filter['column'], $filter['value']); 
                    break;  
                default:
                    $query->where($filter['column'], $mod, $filter['value']);
                    break;
            }
        }
        return $query->get();
    }

    public function getModel() {
        return $this->model;
    }
}
