<?php

namespace App\Services;

use App\Models\Operation;
use App\Models\OperationType;
use Illuminate\Support\Facades\Auth;

class OperationService extends BaseService
{
    protected $model = Operation::class;

    public function store(array $data): Operation 
    {   
        $operation = Operation::create($data);

        return $operation;
    }

    public function generateSecurityCode(int $operationTypeId): string 
    {   
        $user = Auth::user();
        $service = new OperationTypeService();
        $operationType = $service->getModel()::find($operationTypeId);
        $count = $this->getByCustom([
            ['column' => "user_from_id", "value" => $user->id],
            ['column' => "operation_type_id", "value" => $operationTypeId],
        ])->count();
        return $operationType->code . str_pad($count, 4, "0", STR_PAD_LEFT);
    }

    public function confirmDeposit(Operation $operation): bool 
    {
        $userTo = $operation->userTo;
        $userTo->balance += $operation->value;
        $userTo->save();
        $operation->is_pending = false;
        $operation->save();
        
        return true;
    }
    
    public function confirmTransfer(Operation $operation): bool 
    {
        $userFrom = $operation->userFrom;
        $userFrom->balance -= $operation->value;
        $userFrom->save();
        
        $userTo = $operation->userTo;
        $userTo->balance += $operation->value;
        $userTo->save();
        
        $operation->is_pending = false;
        $operation->save();
        
        return true;
    }
}