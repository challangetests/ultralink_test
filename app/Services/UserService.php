<?php

namespace App\Services;

use App\Services\BaseService;
use App\Models\User;

class UserService extends BaseService
{
    protected $model = User::class;

    public function store(array $data): User 
    {
        unset($data['balance']);
        $service = new ViacepService();
        $zipCodeData = $service->getZipCodeData($data['zip_code']);
        $data = array_merge($data, $zipCodeData);
        $user = User::create($data);

        return $user;
    }
}