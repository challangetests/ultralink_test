<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Http;

class ViacepService 
{
    protected $model = User::class;

    public function getZipCodeData(string $cep): array 
    {
        $response = Http::withHeaders([
            'Accept' => 'application/json',
            'App' => env('APP_ID')
        ])->get(env('VIACEP_URL') . $cep .'/json')->json();

        return [
            "address" => $response['logradouro'],
            "address_complement" => $response['complemento'],
        ];
    }
}