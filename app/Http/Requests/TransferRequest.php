<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TransferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|string|exists:users,email',
            'value' => 'required|decimal:2|gt:0|lt:1000000000',
        ];
    }

    public static function selfTransfer(string $email): bool
    {
        return Auth::user()->email == $email;
    }

    public static function enoughBalance(string $value): bool
    {
        return Auth::user()->balance >= $value;
    }
}
