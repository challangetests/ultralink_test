<?php

namespace App\Http\Controllers;

use App\Helpers\AuthHelper;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class AuthController extends BaseController
{
   private UserService $userService;

   public function __construct(UserService $userService) {
      $this->userService = $userService;
   }
   
   public function register(RegisterRequest $request): JsonResponse
   {
      $data = $request->all();
      if (!RegisterRequest::validateCpf($data['cpf'])) {
         return response()->json(
            [
               'message' => __('validation.validation_error'),
               'errors' => ['cpf' => [__('validation.cpf')]]
            ],
            JsonResponse::HTTP_UNPROCESSABLE_ENTITY
         );
      }

      try {         
         $user = $this->userService->store($data);

         return response()->json([
            "message" => __('crud.message.insert', [ "model" => __('crud.model.user.label')]),
            "user" => new UserResource($user),
            "token" => AuthHelper::generateAcessToken($user)
         ], JsonResponse::HTTP_OK);
      } catch (Exception $e) {
         return response()->json(
            ["message" => $e->getMessage()], 
            JsonResponse::HTTP_INTERNAL_SERVER_ERROR
         );
      }
   }

   public function login(LoginRequest $request): JsonResponse
   {
      $token = AuthHelper::login($request->all());
      if (!is_null($token)) {        
         return response()->json([
               "message" => __('auth.success'),
               "token" => $token
            ], JsonResponse::HTTP_OK
         );
      }

      return response()->json(
         ["message" => __('auth.failed')], 
         JsonResponse::HTTP_UNAUTHORIZED
     );
   }
}