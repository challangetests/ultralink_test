<?php

namespace App\Http\Controllers;

use App\Http\Requests\DepositRequest;
use App\Models\OperationType;
use App\Services\OperationService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class DepositController extends BaseController
{
   private UserService $userService; 
   private OperationService $operationService;

   public function __construct(
      UserService $userService,
      OperationService $operationService,
   ) {
      $this->userService = $userService;
      $this->operationService = $operationService;
   }
   
   public function store(DepositRequest $request): JsonResponse
   {
      $data = $request->all();    

      try {      
         DB::beginTransaction();
         $userTo = $this->userService->getByCustom([['column' => "email", "value" => $data['email']]])->first();
         $data['operation_type_id'] = OperationType::DEPOSIT;
         $data['user_from_id'] = Auth::user()->id;
         $data['user_to_id'] = $userTo->id;
         $data['code'] = $this->operationService->generateSecurityCode(OperationType::DEPOSIT);

         $operation = $this->operationService->store($data);
         DB::commit();
         return response()->json([
            "message" => __('crud.message.insert', [ "model" => __('crud.model.operation.type.deposit')]),
            "code" => $operation->code
         ], JsonResponse::HTTP_OK);
      } catch (Exception $e) {
         DB::rollBack();
          return response()->json(
              ["message" => $e->getMessage()], 
              JsonResponse::HTTP_INTERNAL_SERVER_ERROR
          );
      }
   }

   public function confirm(Request $request, $code): JsonResponse
   {
      try { 
         $operation = $this->operationService->getByCustom([
            ['column' => "code", "value" => $code],
            ['column' => "user_from_id", "value" => Auth::user()->id],
            ['column' => "is_pending", "value" => true],
            ['column' => "operation_type_id", "value" => OperationType::DEPOSIT],
         ])->first();
        
         if (is_null($operation)) {        
            return response()->json(
               [
                  'message' => __('crud.message.object_not_found', [ "object" => __('crud.model.operation.type.deposit')]),
               ],
               JsonResponse::HTTP_NOT_FOUND
            );
         }

         DB::beginTransaction();
         $this->operationService->confirmDeposit($operation);
         DB::commit();

         return response()->json(
            ["message" =>  __('crud.message.object_approved', [ "object" => __('crud.model.operation.type.deposit')])],
            JsonResponse::HTTP_OK
         );
      } catch (Exception $e) {
         DB::rollBack();
         return response()->json(
            ["message" => $e->getMessage()], 
            JsonResponse::HTTP_INTERNAL_SERVER_ERROR
         );
      }
   }
}