<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "email" => $this->email,
            'birthdate' => $this->birthdate,
            'cpf' => $this->cpf,
            'address' => $this->address,
            'zip_code' => $this->zip_code,
            'address_complement' => $this->address_complement,
            'address_number' => $this->address_number,
        ];
    }
}
