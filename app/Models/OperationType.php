<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OperationType extends Model
{
    use HasFactory;

    public const DEPOSIT = 1;
    public const TRANSFER = 2;

    protected $fillable = [
        'name',
        'code',              
    ];

    public function operations()
    {
        return $this->hasMany(Operation::class);
    }
}
