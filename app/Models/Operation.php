<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    use HasFactory;

    protected $fillable = [
        'operation_type_id',
        'user_from_id',
        'user_to_id',
        'value',
        'code',        
    ];

    public function type()
    {
        return $this->belongsTo(OperationType::class);
    }
 
    public function userFrom()
    {
        return $this->belongsTo(User::class, 'user_from_id');
    } 

    public function userTo()
    {
        return $this->belongsTo(User::class, 'user_to_id');
    }
}
