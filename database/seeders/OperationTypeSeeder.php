<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\OperationType;

use Illuminate\Database\Seeder;

class OperationTypeSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            "states" => [
                "model" => OperationType::class,
                "data" => [
                    ["name" => "Depósito", "code" => "DEP"],
                    ["name" => "Transferência", "code" => "TRANSF"],
                    ["name" => "Saque", "code" => "SAQ"],                    
                ]
            ]
        ];

        foreach ($options as $option) {
            foreach ($option['data'] as $data) {
                $option['model']::insert($data);
            }
        }
    }
}
