<?php

use App\Models\OperationType;
use Database\Seeders\OperationTypeSeeder;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('operation_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code');            
        });
        
        Schema::create('operations', function (Blueprint $table) {
            $table->id();
            
            $table->unsignedBigInteger('operation_type_id');
            $table->foreign('operation_type_id')->references('id')->on('operation_types');
            
            $table->unsignedBigInteger('user_from_id');
            $table->foreign('user_from_id')->references('id')->on('users');
            
            $table->unsignedBigInteger('user_to_id')->nullable();
            $table->foreign('user_to_id')->references('id')->on('users');
            
            $table->decimal('value', 10, 2);
            $table->string('code');
            $table->boolean('is_pending')->default(true);
            $table->timestamps();
        });

        if (OperationType::all()->count() == 0) {
            Artisan::call('db:seed', [
                '--class' => OperationTypeSeeder::class,
            ]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('operations');
        Schema::dropIfExists('operation_types');
    }
};
