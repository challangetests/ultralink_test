<h1> Instalação do projeto <h1>
<p>Na raiz do projeto execute os comandos a seguir</p>
<ol>
    <li>docker-compose up -d</li>
    <li>docker exec php83-fpm bash -it</li>
    <li>cd ultralink_test</li>
    <li>composer install</li>
    <li>php artisan migrate</li>    
</ol>
<p>Sete para o localhost de sua maquina o endereço ultralink_test.desenv</p>
<p>Para adquirir os endpoints acesse a home do projeto ou cheque a coleção postman "endpoints.postman_collection.json" na raiz do projeto.<p>