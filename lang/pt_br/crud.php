<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during crud operations across tghe system.
    |
    */

    'model' => [      
        "user" => [
            "label" => "Usuário",
            "name" => "Nome Completo",
            "email" => "Email",
            "password" => "Senha"           
        ],
        "operation" => [
            "label" => "Operação",
            "code" => "Código de Segurança",
            "type" => [
                "label" => "Tipo de Operação",
                "deposit" => "Depósito",
                "transfer" => "Transferência",
            ]
        ]
    ],
    'message' => [
        "insert" => ":model criado(a) com sucesso.",
        "update" => ":model alterado(a) com sucesso.",
        "delete" => ":model deletado(a) com sucesso.",
        "not_found" => "Objeto não encontrado.",
        "object_not_found" => ":object não encontrado(a).",
        "class_not_found" => "Classe não encontrada.",
        "property_not_found" => "Propridade não encontrada.",
        "import_sucess" => "Arquivo importado com sucesso.",
        "classified" => "Confidencial",
        "object_approved" => ":object aprovado com sucesso",
        "object_reproved" => ":object reprovado com sucesso",
    ],

];
